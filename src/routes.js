import { createBrowserRouter } from 'react-router-dom';

import App from './App';
import Weather from './components/Weaher';
import Counter from './components/Counter';
import Calculator from './components/Calculator';
import Quotes from './components/Quotes';

const router = createBrowserRouter([
	{
		path: '/',
		element: <App />,
	},
	{
		path: 'weather/:id',
		element: <Weather />,
	},
	{
		path: 'weather/',
		element: <Weather />,
	},
	{
		path: 'counter',
		element: <Counter />,
	},
	{
		path: 'calculator',
		element: <Calculator />,
	},
	{
		path: 'quotes',
		element: <Quotes />,
	},
]);

export default router;
