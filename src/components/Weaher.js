import { useState, lazy, Suspense } from 'react';
import { useNavigate } from 'react-router-dom';

import '../assets/styles/weather.css';

const Info5Days = lazy(() => import('./Info5days'));

function Weather() {
	const [city, setCity] = useState('');
	const [data, setData] = useState({});

	const navigate = useNavigate();

	function getCoordinates() {
		fetch(`${process.env.REACT_APP_API_URL}/weather?q=${city}&appid=${process.env.REACT_APP_API_KEY}&units=metric&lang=ru`)
			.then((res) => res.json())
			.then((data) => {
				loadWeatherInfo(data.coord);
			});
	}

	function loadWeatherInfo({ lon, lat }) {
		fetch(`${process.env.REACT_APP_API_URL}/forecast?lat=${lat}&lon=${lon}&appid=${process.env.REACT_APP_API_KEY}&units=metric&lang=ru`)
			.then((res) => res.json())
			.then((data) => {
				setData(data);
			});
	}
	// eslint-disable-next-line
	// const memoComp = useMemo(() => <Info5Days data={data} />, [data]);

	return (
		<div className="weather">
			<button onClick={() => navigate('/')}>Back</button>

			<div>
				<input className="weather__input" placeholder="Enter city name" onChange={(e) => setCity(e.target.value)} />
				<button className="weather__btn" onClick={getCoordinates}>
					Search
				</button>
			</div>
			<Suspense fallback="Loading...">
				<Info5Days data={data} />
			</Suspense>
		</div>
	);
}
export default Weather;
