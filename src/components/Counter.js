import { useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { Link } from 'react-router-dom';

import { increment, decrement, incrementByAmount } from '../store/reducer/counter';

import { createTheme, alpha, ThemeProvider, getContrastRatio } from '@mui/material/styles';
import { Button, Box } from '@mui/material';

function Counter() {
	// const [state, dispatch] = useReducer(reducer, initialState);
	const { count } = useSelector((state) => state.counter);
	const dispatch = useDispatch();

	const [mode, setMode] = useState('light');
	const violetBase = '#7F00FF';
	const violetMain = alpha(violetBase, 0.7);

	const darktheme = createTheme({
		palette: {
			mode,
			violet: {
				main: violetMain,
				light: alpha(violetBase, 0.5),
				dark: alpha(violetBase, 0.9),
				contrastText: getContrastRatio(violetMain, '#fff') > 4.5 ? '#fff' : '#111',
			},
			ochre: {
				main: '#E3D026',
				light: '#E9DB5D',
				dark: '#A29415',
				contrastText: '#242105',
			},
		},
	});

	const lighttheme = createTheme({
		palette: {
			mode,
			violet: {
				main: alpha(violetBase, 0.9),
				light: alpha(violetBase, 0.5),
				dark: alpha(violetBase, 0.9),
				contrastText: getContrastRatio(violetMain, '#fff') > 4.5 ? '#fff' : '#111',
			},
			ochre: {
				main: '#A29415',
				light: '#E9DB5D',
				dark: '#A29415',
				contrastText: '#242105',
			},
		},
	});

	return (
		<ThemeProvider theme={mode === 'light' ? lighttheme : darktheme}>
			<Box bgcolor="secondary">
				<Link to="/"> Go back</Link>
				<hr />
				<Button variant="contained" color="violet" onClick={() => dispatch(decrement())}>
					-1
				</Button>
				<h1>{count}</h1>
				<Button variant="contained" onClick={() => dispatch(increment())}>
					+1
				</Button>
				<Button variant="contained" onClick={() => dispatch(incrementByAmount(100))}>
					+100
				</Button>
				<hr />
				<Button onClick={() => (mode === 'light' ? setMode('dark') : setMode('light'))}>Switch theme</Button>
			</Box>
		</ThemeProvider>
	);
}

export default Counter;
