import { useEffect, useState } from 'react';
import '../assets/styles/info.css';

function Info5Days({ data }) {
	const [formattedData, setFormattedData] = useState([]);

	useEffect(() => {
		const info = {};

		data.list?.forEach((item) => {
			let date = item.dt_txt.split(' ');

			if (!info[date[0]]) {
				let weatherlist = data.list.filter((elem) => elem.dt_txt.includes(date[0]));

				let weatherObj = {};
				for (let weather of weatherlist) {
					let weatherDate = weather.dt_txt.split(' ');
					weatherObj[weatherDate[1]] = weather;
				}
				info[date[0]] = weatherObj;
			}
		});

		const array = Object.entries(info).map(([key, value]) => [key, value]);
		setFormattedData(array);
	}, [data.list]);

	return (
		<div className="info-block">
			<h1 className="info-block__title">City: {data?.city?.name} </h1>
			<div className="info-block__row">
				{formattedData?.map((item, i) => (
					<div className="info-block__item" key={i}>
						<p className="info-block__calendar">{new Date(item[0]).toDateString()}</p>
						<img
							className="info-block__img"
							src={`https://openweathermap.org/img/wn/${
								item[1]['12:00:00']?.weather[0]?.icon || item[1]['15:00:00']?.weather[0]?.icon
							}@2x.png`}
							alt="weather"
						/>
						<h2 className="info-block__desc">
							Day: {item[1]['12:00:00']?.main?.temp || item[1]['15:00:00']?.main?.temp} &#8451;
						</h2>
						<h2 className="info-block__desc">
							Night: {item[1]['21:00:00']?.main?.temp || item[1]['00:00:00']?.main?.temp} &#8451;
						</h2>
					</div>
				))}
			</div>
		</div>
	);
}

export default Info5Days;
