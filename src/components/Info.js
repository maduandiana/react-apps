import { useState, useEffect } from 'react';
import '../assets/styles/info.css';

function Info({ data }) {
	const [date, setDate] = useState(new Date().toDateString());

	useEffect(() => {
		if (data.dt) {
			let date = new Date(data.dt * 1000);
			setDate(date.toDateString());
		}
	}, [data.dt]);

	return (
		<div className="info-block">
			<p className="info-block__calendar">{date}</p>
			<h1 className="info-block__title">{data.name}</h1>
			<h2 className="info-block__temp">{data?.main ? Math.round(data?.main.temp) : '-'} C</h2>

			<div className="info-block__row">
				{data.weather?.map((item) => (
					<div className="info-block__item" key={item.main}>
						<p className="info-block__desc">{item.main}</p>
						<img className="info-block__img" src={`https://openweathermap.org/img/wn/${item.icon}@2x.png`} alt="weather" />
						<p className="info-block__desc">{item.description}</p>
					</div>
				))}
			</div>
		</div>
	);
}

export default Info;
