import { useSelector } from 'react-redux';
import { Button } from '@mui/material';
import { loadRandomQuotes } from '../store/actions/quotes';

const Quotes = () => {
	const { quote } = useSelector((state) => state.quotes);
	return (
		<div>
			<p>{quote.quote}</p>
			<hr />
			<p>{quote.author}</p>
			<Button onClick={() => loadRandomQuotes('business')}>Generate new quotes</Button>
		</div>
	);
};

export default Quotes;
