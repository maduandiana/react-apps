import { useMemo, useState } from 'react';

import Box from '@mui/material/Box';
import OutlinedInput from '@mui/material/OutlinedInput';
import InputLabel from '@mui/material/InputLabel';
import InputAdornment from '@mui/material/InputAdornment';
import FormHelperText from '@mui/material/FormHelperText';
import FormControl from '@mui/material/FormControl';
import { Stack, Slider, Typography, Select, MenuItem } from '@mui/material';

import '../assets/styles/calculator.css';
import { useSelector, useDispatch } from 'react-redux';
import { setMonth, setHouseCost, setDeposit } from '../store/reducer/calculator';

function Calculator() {
	const { month, deposit, houseCost } = useSelector((state) => state.calculator);
	const [city, setCity] = useState('');
	const dispatch = useDispatch();

	const percent = useMemo(() => {
		if (month < 53) {
			return 17.5;
		} else if (month < 110) {
			return 17.4;
		} else {
			return 17.3;
		}
	}, [month]);

	// eslint-disable-next-line
	const monthlyPayment = useMemo(() => calculateMonthlyPayment(), [houseCost, deposit, month, percent]);

	const monthValues = [
		{
			value: 36,
			label: '36',
		},
		{
			value: 240,
			label: '240',
		},
	];

	const cities = [
		{
			logo: 'astana',
			name: 'Astana',
		},
		{
			logo: 'astana',
			name: 'Almaty',
		},
	];

	function calculateMonthlyPayment() {
		// Преобразование годовой ставки в месячную и расчет количества месяцев
		const monthlyInterestRate = percent / 100 / 12;
		const numberOfPayments = +month;

		// Формула аннуитетного платежа: P = [r * PV] / [1 - (1 + r)^-n]
		const numerator = monthlyInterestRate * (houseCost - deposit);
		const denominator = 1 - Math.pow(1 + monthlyInterestRate, -numberOfPayments);

		const monthlyPayment = numerator / denominator;

		return monthlyPayment.toFixed();
	}

	return (
		<Box sx={{ display: 'flex', flexWrap: 'wrap', justifyContent: 'space-around', m: 4 }}>
			<div>
				<Stack direction="row" spacing={2}>
					<FormControl sx={{ width: '35ch' }} variant="outlined">
						<InputLabel htmlFor="outlined-adornment-weight">Стоимость жилья</InputLabel>
						<OutlinedInput
							id="outlined-adornment-weight"
							label="Стоимость жилья"
							value={houseCost}
							onChange={(e) => dispatch(setHouseCost(e.target.value))}
							endAdornment={<InputAdornment position="end">₸</InputAdornment>}
							aria-describedby="outlined-weight-helper-text"
							inputProps={{
								'aria-label': 'weight',
							}}
						/>
						<FormHelperText id="outlined-weight-helper-text">от 3 750 000</FormHelperText>
					</FormControl>
					<FormControl sx={{ width: '35ch' }} variant="outlined">
						<InputLabel htmlFor="outlined-adornment-weight-1">Первоначальный взнос</InputLabel>
						<OutlinedInput
							id="outlined-adornment-weight-1"
							label="Первоначальный взнос"
							value={deposit}
							onChange={(e) => dispatch(setDeposit(e.target.value))}
							endAdornment={<InputAdornment position="end">₸</InputAdornment>}
							aria-describedby="outlined-weight-helper-text"
							inputProps={{
								'aria-label': 'weight',
							}}
						/>
						<FormHelperText id="outlined-weight-helper-text">от 750 000</FormHelperText>
					</FormControl>
				</Stack>
				<FormControl sx={{ mt: 1 }} fullWidth variant="outlined">
					<InputLabel htmlFor="outlined-adornment-weight-1">Срок займа</InputLabel>
					<OutlinedInput
						id="outlined-adornment-weight-1"
						label="Срок займа"
						type="number"
						value={month}
						onChange={(e) => dispatch(setMonth(e.target.value))}
						endAdornment={<InputAdornment position="end">month</InputAdornment>}
						aria-describedby="outlined-weight-helper-text"
						inputProps={{
							'aria-label': 'weight',
						}}
					/>

					<Slider
						aria-label="Month"
						sx={{ mt: -2 }}
						min={36}
						step={1}
						max={240}
						onChange={(e, newValue) => dispatch(setMonth(newValue))}
						marks={monthValues}
						value={month}
						color="primary"
					/>
				</FormControl>
				<FormControl sx={{ m: 1, width: 300 }}>
					<InputLabel id="demo-multiple-name-label">Name</InputLabel>
					<Select
						labelId="demo-multiple-name-label"
						id="demo-multiple-name"
						value={city}
						onChange={(e) => setCity(e.target.value)}
						input={<OutlinedInput label="Name" />}
					>
						{cities.map(({ logo, name }) => (
							<MenuItem key={name} value={name}>
								<img src={`/images/icons/${logo}.svg`} alt={name} className="city-icon" />
								{name}
							</MenuItem>
						))}
					</Select>
				</FormControl>
			</div>
			<Box sx={{ m: 2 }}>
				<p>Ежемесячный платеж</p>

				<Typography variant="h3" component="h2">
					{monthlyPayment} ₸
				</Typography>
				<Stack direction="row" spacing={3} sx={{ mt: 3 }}>
					<div>
						<p>Сумма кредита</p>
						<Typography variant="h5" component="h5">
							{houseCost - deposit} ₸
						</Typography>
					</div>
					<div>
						<p>Эффективная ставка</p>
						<Typography variant="h5" component="h5">
							от {percent} %
						</Typography>
					</div>
				</Stack>
			</Box>
		</Box>
	);
}

export default Calculator;
