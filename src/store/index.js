import { configureStore } from '@reduxjs/toolkit';
import counter from './reducer/counter';
import calculatorReducer from './reducer/calculator';
import quotesReducer from './reducer/quotes';

export default configureStore({
	reducer: {
		counter,
		quotes: quotesReducer,
		calculator: calculatorReducer,
	},
});
