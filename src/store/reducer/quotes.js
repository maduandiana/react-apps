import { createSlice } from '@reduxjs/toolkit';

export const quotesSlice = createSlice({
	name: 'quotes',
	initialState: {
		quote: {},
	},
	reducers: {
		setQuote: (state, action) => {
			state.quote = action.payload;
		},
	},
});

// Action creators are generated for each case reducer function
export const { setQuote } = quotesSlice.actions;

export default quotesSlice.reducer;
