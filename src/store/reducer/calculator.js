import { createSlice } from '@reduxjs/toolkit';

export const calculatorSlice = createSlice({
	name: 'calculator',
	initialState: {
		month: 36,
		houseCost: 3750000,
		deposit: 750000,
	},
	reducers: {
		setMonth: (state, action) => {
			state.month = action.payload;
		},
		setHouseCost: (state, action) => {
			state.houseCost = action.payload;
		},
		setDeposit: (state, action) => {
			state.deposit = action.payload;
		},
	},
});

// Action creators are generated for each case reducer function
export const { setMonth, setHouseCost, setDeposit } = calculatorSlice.actions;

export default calculatorSlice.reducer;
