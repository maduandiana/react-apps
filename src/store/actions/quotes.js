import store from '../index';
import { setQuote } from '../reducer/quotes';
import axios from 'axios';

const customAxios = axios.create({
	headers: { 'X-Api-Key': 'Your api key' },
	timeout: 5000,
});

const loadRandomQuotes = (category) => {
	// fetch(`https://api.api-ninjas.com/v1/quotes?category=${category}`, {
	// 	headers: { 'X-Api-Key': 'Your api key' },
	// })
	// 	.then((res) => res.json())
	// 	.then((data) => store.dispatch(setQuote(data[0])));
	customAxios.get(`https://api.api-ninjas.com/v1/quotes?category=${category}`).then((result) => store.dispatch(setQuote(result.data[0])));
};

export { loadRandomQuotes };
