import { Link } from 'react-router-dom';

import './assets/styles/App.css';

function Home() {
	return (
		<div className="menu">
			<Link to="weather">Weather app</Link>
			<Link to="counter">Counter app</Link>
			<Link to="calculator">Calculator app</Link>
			<Link to="quotes">Quotes generator</Link>
		</div>
	);
}

export default Home;
